module.exports = function (self) {
  self.contents = this.optional.string;
  self.file = this.optional.File;
  self.mediaType = this.optional.MediaType;

  if (self.file) {
    self.file.read(function () {
      self.contents = this.string;
    });
  }
};
